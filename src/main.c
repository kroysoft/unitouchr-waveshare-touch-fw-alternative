/**
  Custom firmware for WaveShare 5" rev(b) resistive touchscreen display
  Right to copy (C) 2017-2019 Karoly Kovacs / Forge Limited

  this firmware, however is heavily based on the following code
  (except for the xpt2046 part):

  Open firmware for Waveshare 7" capacitive touchscreen
  Copyright (C) 2016 Yannic Staudt / Staudt Engineering


  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>.

 */

#include <stdlib.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/cm3/systick.h>
#include <scb.h>

#include "usb_device.h"
#include "systick.h"
#include "xpt2046.h"

int main (void)
{
  // setup the CPU
  rcc_clock_setup_in_hse_8mhz_out_72mhz();    // 72MHz clock using on-board crystal / HSE
  rcc_set_usbpre (RCC_CFGR_USBPRE_PLL_CLK_DIV1_5);

  // start the clock
  setup_systick();

  rcc_periph_clock_enable (RCC_GPIOA);
  /*
   * This is a somewhat common cheap hack to trigger device re-enumeration
   * on startup.  Assuming a fixed external pullup on D+, (For USB-FS)
   * setting the pin to output, and driving it explicitly low effectively
   * "removes" the pullup.  The subsequent USB init will "take over" the
   * pin, and it will appear as a proper pullup to the host.
   * The magic delay is somewhat arbitrary, no guarantees on USBIF
   * compliance here, but "it works" in most places.
   */
  gpio_set_mode (GPIOA, GPIO_MODE_OUTPUT_50_MHZ,
                 GPIO_CNF_OUTPUT_PUSHPULL, GPIO12);
  gpio_clear (GPIOA, GPIO12);

  // wait for the power transients to settle...
  msleep (10);

  // setup the SPI bus and configure the XPT2046 touchscreen controller
  setup_xpt2046();

  setup_usb();


  while (1)
    {
      int16_t ctime, ptime;

      poll_usb();
      ctime = get_scan_time();  // get current systick
      if (ptime != ctime)       // has a tick passed?
      {
        ptime = ctime;          // save current time as previous time
        xpt2046_poll();         // call this in every millisecond - but not any often!
      }
    }

  scb_reset_system();
}
