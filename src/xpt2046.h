#ifndef XPT2046_H_INCLUDED
#define XPT2046_H_INCLUDED

#define TOUCH_POINTS_MAX 1

#define XPT_RELEASE() gpio_set(GPIOB,GPIO7)
#define XPT_SELECT()  gpio_clear(GPIOB,GPIO7)

void setup_xpt2046 (void);
void xpt2046_poll (void);
void init_reports (void);

#endif
