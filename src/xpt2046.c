#include <stdlib.h>
#include <libopencm3/stm32/spi.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/rcc.h>
#include "usb_device.h"
#include "systick.h"
#include "xpt2046.h"

#define xpt_combine(res, h,l)   res=((h << 4) | (l >> 4)) // remove LSB!

#define CTRL_Z1                 0xB3    // Z1 address in DFR mode
#define CTRL_Z2                 0xC3    // Z2 address in DFR mode
#define CTRL_X                  0xD3    // X address in DFR mode
#define CTRL_Y                  0x93    // Y address in DFR mode
#define CTRL_T                  0x84    // int. temperature reg. addr., SFR mode
#define CTRL_0                  0x00    // used for flushing readouts

#define ZD_THRESHOLD            3850
#define Z1_THRESHOLD            96
#define MAX_SAMPLES             16

#define RAW_FILTER_LVL          5   // filter attenuation, must be set between 1 and 6
#define COORD_FILTER_LVL        2

#define MAX_COORD_ERROR         12  // cumulative touch error limit on both axis; absolute value

// this is still far from perfect but it works, more-or-less. Heavy improvements could be made, however...
#define TOUCH_DEBOUNCE_COUNT    3   // before sending the first 'touch down' event, we'll skip some report occasion before sending actual coordinates, but we do the filtering anyway
#define RELEASE_DEBOUNCE_MS     48  // after sending a 'touch up' event, we'll wait a while before sending anything again

static unsigned int uiADFlt[4] = {0, 0, 0, 0};
static int16_t x, y, lastx, lasty, x_error, y_error;

///
// stores one touch report and the information if the
// "touch up" event was already send for this fingers
// current touch sequence
struct touch_report
{
  uint8_t report[11];
  bool touch_up_sent;   // remember if we already sent a touch up report for this finger
};

///
// buffers for all fingers worth of HID reports
struct touch_report touch_reports[TOUCH_POINTS_MAX];
struct touch_report* hid_report;

///
// XPT2046 receive buffer
uint16_t data[10];

static unsigned int
InitLowPass (unsigned char ch, unsigned int data, uint8_t flt_lvl)
{
  unsigned int uiTemp = data << flt_lvl;
  uiADFlt[ch] = uiTemp;
  return (uiTemp >> flt_lvl);
}

static unsigned int
LowPass (unsigned char ch, unsigned int data, uint8_t flt_lvl)
{
  unsigned int uiTemp = uiADFlt[ch];

  uiTemp = uiTemp - (uiTemp >> flt_lvl) + data;
  uiADFlt[ch] = uiTemp;
  return (uiTemp >> flt_lvl);

}

static uint8_t spi_transcieve (uint32_t spi, uint8_t data)
{
  spi_send (spi, data);

  /* Wait for transfer finished. */
  while (! (SPI_SR (spi) & SPI_SR_RXNE));

  /* Read the data (8 or 16 bits, depending on DFF bit) from DR. */
  return SPI_DR (spi);
}

///
// setup routine
void setup_xpt2046 (void)
{
  /** setup GPIO */
  rcc_periph_clock_enable (RCC_GPIOA);    // enable clock for IO port A
  rcc_periph_clock_enable (RCC_GPIOB);    // enable clock for IO port B

  spi_reset (SPI1);
  /** setup SPI1 */
  rcc_periph_clock_enable (RCC_SPI1);     // Enable clocks for SPI1
  rcc_periph_clock_enable (RCC_AFIO);     // and AFIO

  gpio_set_mode (GPIOA,                   // Set alternate functions for the SCK, MISO and MOIS pins of SPI1.
                 GPIO_MODE_OUTPUT_50_MHZ,
                 GPIO_CNF_OUTPUT_ALTFN_PUSHPULL,
                 GPIO_SPI1_SCK | GPIO_SPI1_MISO | GPIO_SPI1_MOSI);

  gpio_set_mode (GPIOB,                   // Set pushpull function for the CS
                 GPIO_MODE_OUTPUT_50_MHZ,
                 GPIO_CNF_OUTPUT_PUSHPULL,
                 GPIO7);
  XPT_RELEASE();

  spi_disable (SPI1);
  spi_disable_crc (SPI1);
  spi_disable_error_interrupt (SPI1);
  spi_disable_rx_buffer_not_empty_interrupt (SPI1);
  spi_disable_rx_dma (SPI1);
  spi_enable_software_slave_management (SPI1);
  spi_disable_ss_output (SPI1);
  spi_disable_tx_buffer_empty_interrupt (SPI1);
  spi_disable_tx_dma (SPI1);

  spi_set_baudrate_prescaler (SPI1, 4); // set ~1MHz SPI clock

  spi_set_clock_phase_1 (SPI1);
  spi_set_clock_polarity_1 (SPI1);
  SPI_CR1 (SPI1) &= ~SPI_CR1_DFF;
  spi_send_msb_first (SPI1);
  spi_set_full_duplex_mode (SPI1);
  spi_set_nss_high (SPI1);
  spi_set_master_mode (SPI1);
  spi_enable (SPI1);

  XPT_SELECT();
  spi_transcieve (SPI1, CTRL_T); // dummy temperature readout
  spi_transcieve (SPI1, CTRL_0);
  spi_transcieve (SPI1, CTRL_0);
  XPT_RELEASE();

  init_reports();
}

void init_reports (void)
{
  unsigned int i;
  /* initialize the touch reports struct */
  for (i = 0; i < TOUCH_POINTS_MAX; i++)
    {
      // point to the current finger's report
      hid_report = &touch_reports[i];

      // avoid being caught in a dumb infinite loop in the beginning...
      hid_report->touch_up_sent = true;
    }

  // clear the data from previous loop iteration
  for (i = 0; i < (sizeof (data) / sizeof (uint16_t)); i++)
    data[i] = 0;
}

static uint16_t _readLoop (uint8_t ctrl)
{
  register uint16_t prev = 0xffff, cur = 0xffff;
  register uint8_t i = 0;
  do
    {
      prev = cur;
      cur = spi_transcieve (SPI1, CTRL_0);
      xpt_combine (cur, cur, spi_transcieve (SPI1, ctrl)); // 16 clocks -> 12-bits (zero-padded at end)
    }
  while ( (prev != cur) && (++i < MAX_SAMPLES));

  return cur << 1;
}

static bool xpt2046_read (uint16_t* xyz)
{
  register uint16_t z1, z2;
  static bool wasreleased = true;
  XPT_SELECT();

  //Check if touch screen is pressed.
  spi_transcieve (SPI1, CTRL_Z1);   // start to read Z1
  z1 = _readLoop (CTRL_Z1);         // save Z1
  z2 = _readLoop (CTRL_Z2);         // save Z2
  spi_transcieve (SPI1, CTRL_0);    // flush
  spi_transcieve (SPI1, CTRL_0);

  xyz[2] = (z2 - z1);
  xyz[3] = z1;
  xyz[4] = z2;

  if ( (xyz[2] < ZD_THRESHOLD) && (z1 >= Z1_THRESHOLD)) // If the touch screen is pressed, read the X,Y  coordinates from XPT2046.
    {
      spi_transcieve (SPI1, CTRL_X);  // start to read X
      xyz[6] = _readLoop (CTRL_X);    // save raw X
      xyz[7] = _readLoop (CTRL_Y);    // save raw Y
      xyz[5] = _readLoop (CTRL_T);    // save temp, start to flush readouts

      spi_transcieve (SPI1, CTRL_0);  // flush
      spi_transcieve (SPI1, CTRL_0);

      if (wasreleased)                // is this a new touch?
        {
          wasreleased = false;
          xyz[0] = InitLowPass (0, xyz[6], RAW_FILTER_LVL); // then init filter
          xyz[1] = InitLowPass (1, xyz[7], RAW_FILTER_LVL);

          XPT_RELEASE();
          return false;               // report as if it was not touched yet
        }
      else
        {
          xyz[0] = LowPass (0, xyz[6], RAW_FILTER_LVL);   // otherwise apply digital low-pass filter to get rid of cursor shaking
          xyz[1] = LowPass (1, xyz[7], RAW_FILTER_LVL);

          XPT_RELEASE();
        }
      return true;
    }
  wasreleased = true;
  XPT_RELEASE();
  return false;
}

//
// poll the XPT2046 for touch and
// split the result in HID reports to send to our controller
// TODO: split function to a poll funcion of the XPT2046 AND an HID report handler, separately
void xpt2046_poll (void)
{
  uint16_t scan_time = get_scan_time();
  bool istouched = xpt2046_read (data);
  static int debounce[TOUCH_POINTS_MAX] = {0};
  static uint16_t skip = 0;


  if (((scan_time & 0x07) == 0) && !debounce[0])  // send in every 8th ticks, which means 125Hz update rate
    {
      uint8_t currentFinger, validTouchPoints, offsetData, sentReports;

      // init some variables
      offsetData = 0;
      validTouchPoints = 0;
      sentReports = 0;

      // find out how many touches we have:
      if (istouched)
        validTouchPoints++;
      /*for (currentFinger = 0; currentFinger < TOUCH_POINTS_MAX; currentFinger++)
        {
          // check if the "finger N" bit is set high in XPT2046 answer
          if ( (data[0] & (0b00000001 << currentFinger)) > 0)
            {
              validTouchPoints++;
            }
          else if (hid_report->touch_up_sent == false)
            {
              // last frame contained a contact point for this finger;
              // => we must send a "touch up" report, which is considered
              // a valid touch point...
              validTouchPoints++;
            }
        }*/

      // send one report per finger found
      for (currentFinger = 0; currentFinger < TOUCH_POINTS_MAX; currentFinger++)
        {
          // point to the current finger's report
          hid_report = &touch_reports[currentFinger];

          // byte[0] = 0x01 (Report ID, for this application, always = 0x01)
          hid_report->report[0] = 0x01;

          // check if the "finger N" bit is set high in XPT2046 answer
          if (validTouchPoints)
            {
              // get actual coordinates
//              x = (uint16_t) ( (800 * (uint32_t) (data[0 + offsetData] - 96)) / 3980); // screen width * (RAW_X - min_raw_x) / (max_raw_x-min_raw_x)
//              x = x < 0 ? 0 : (x > 800 ? 800 : x);                                      // limit min & max
//              y = (uint16_t) ( (480 * (uint32_t) (data[1 + offsetData] - 192)) / 3750); // actual values here are just empirical
//              y = y < 0 ? 0 : (y > 480 ? 480 : y);

              x = (uint16_t) ( (800 * (uint32_t) (data[0 + offsetData] - 110)) / 3890); // screen width * (RAW_X - min_raw_x) / (max_raw_x-min_raw_x)
              x = x < 0 ? 0 : (x > 800 ? 800 : x);                                      // limit min & max
              y = (uint16_t) ( (480 * (uint32_t) (data[1 + offsetData] - 240)) / 3650); // actual values here are just empirical
              y = y < 0 ? 0 : (y > 480 ? 480 : y);

              if (hid_report->touch_up_sent == true)        // first touch since a while?
                {
                  x = InitLowPass (2, x, COORD_FILTER_LVL); // reset filters
                  y = InitLowPass (3, y, COORD_FILTER_LVL);
                  lastx = x;                                // and reset all our helper variables too
                  lasty = y;
                  x_error = 0;
                  y_error = 0;
                  // actually, we'll just skip sending the very first report, because the next readout of coordinates may result in quite different from these
                  hid_report->touch_up_sent = false;
                  skip = TOUCH_DEBOUNCE_COUNT;
                  continue;
                }
              else
                {
                  x = LowPass (2, x, COORD_FILTER_LVL); // filter out high frequency ADC noises and human hand shaking effects...
                  y = LowPass (3, y, COORD_FILTER_LVL);

                  if (skip != 0)
                  {
                    --skip;
                    continue;
                  }
                  x_error += lastx - x;                 // but we further need to calculate delta movements then add this to the signed error value
                  y_error += lasty - y;                 // to filter at lower frequencies:

                  x = (abs(x_error) < MAX_COORD_ERROR)? lastx : x;  // if the cumulative error is below the limit, then we don't send actual position but rather
                  y = (abs(y_error) < MAX_COORD_ERROR)? lasty : y;  // the old ones, thus we effectively filter the shaking of hands at lower frequencies as well

                  if (lastx != x)                       // half the error counters if necessary
                      x_error /= 2;                     // this will eliminate 'jagged' moving on fast coordinate changes
                  if (lasty != y)                       // but still keeps the pointer steady in case of small deviations
                      y_error /= 2;

                  lastx = x;                           // save the new coordinates as the last (sent) one
                  lasty = y;
                }
              // byte[1] = state
              // .......x: Tip switch
              // xxxxxxx.: Ignored
              hid_report->report[1] = 0b00000001;

              // this is still within a move sequence of reports
              hid_report->touch_up_sent = false;
            }
          else if (hid_report->touch_up_sent == false)
            {
              // we're required to send a "touch up" report when
              // a finger was lifted. This means that after the last
              // report with TIP = true was sent we need to issue
              // yet another report for the same finger with the
              // last coordinates but TIP = false
              hid_report->report[1] = 0b00000000;

              // taking care of sending only one touch up report
              hid_report->touch_up_sent = true;
              debounce[currentFinger] = RELEASE_DEBOUNCE_MS;
            }
          else
            continue;   // skip this "finger"

          if (hid_report->touch_up_sent == false)
            {
              // calculate the "data offset" in the XPT2046 data_align
              // for the current finger / at 5 bytes per finger
              offsetData = currentFinger * 5;

              // byte[2] Contact index
              hid_report->report[2] = currentFinger;

              // byte[3] Tip pressure
              hid_report->report[3] = (uint8_t) (data[3 + offsetData] >> 4);
              hid_report->report[3] = 0xfe - hid_report->report[3];

              // NOTE: apparently the waveshare touch digitizer was mounted
              // mirrored in both directions, this means we "flip" X <-> Y
              // coordinates

              // byte[4] X coordinate LSB
              hid_report->report[4] = (uint8_t) (x & 0xff);

              // byte[5] X coordinate MSB
              hid_report->report[5] = (uint8_t) ( (x & 0xff00) >> 8);

              // byte[6] Y coordinate LSB
              hid_report->report[6] = (uint8_t) (y & 0xff);

              // byte[7] Y coordinate MSB
              hid_report->report[7] = (uint8_t) ( (y & 0xff00) >> 8);

              // byte[8] contact count
              if (sentReports == 0)
                {
                  // this is the first report in the frame: write touch count in report
                  hid_report->report[10] = validTouchPoints;
                }
              else
                hid_report->report[10] = 0;
            }

          // bytes[8 & 9] this report's scan time
          hid_report->report[8] = scan_time & 0xff;
          hid_report->report[9] = (scan_time & 0xff00) >> 8;

          // send that report!
//          if (debounce[currentFinger] == 0)
          if (skip == 0) // do not send touch up event before touch down events, accidentally
          {
            send_hid_report (hid_report->report, 11);
            // remember we sent a report
            sentReports++;
          }
//          else if (debounce[currentFinger] > 0)
//            --debounce[currentFinger];

        }
    }
    else if (debounce[0] > 0)
      --debounce[0];
}
