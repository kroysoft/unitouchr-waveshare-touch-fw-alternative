# Welcome to UniTouchR!

 This repository holds an alternative firmware (let's say: a better one than the original) for [WaveShare 5.0" LCD](https://www.waveshare.com/wiki/5inch_HDMI_LCD_(B)) display, which serves the fundamental user I/O interface for my hobby project, the Unicorn embroidery machine.

# Why this sub-project came to life at all?

 Though this display really has a superior quality-price ratio compared to any of its competitors, still it shortly turned out after I tried it at the first time that it had some stability issues on Windows 10 and on Ubuntu Linux 16.04 as well. Since I didn't succeed fixing it only through its configuration alone, I've decided to find another way by writing my firmware for it - at least for the touch input part. Fortunately, at this point I quickly tapped into a good description about an interesting [project](https://k16c.eu/open-firmware-for-waveshares-7-inch-touch-screen/) on the web for a very similar display, which really helped me a lot. Thanks for its author [Yannic](https://k16c.eu/author/yannic/) for sharing his great findings with the rest of the world!

# What do you need for build and flash it on your own:

 * a WaveShare 5 inch LCD, mine is the [B variant](https://www.waveshare.com/5inch-HDMI-LCD-B.htm), other variants may or may not work with this FW.
 * this project is best built with Embitz IDE v1.11, can be downloaded from [here](https://www.embitz.org/EmBitz_1.11.zip)
 * an STM32 compatible SWD probe, like the [ST link V2](https://www.st.com/en/development-tools/st-link-v2.html) or one of its cheap [clones](https://www.aliexpress.com/w/wholesale-stlink-v2.html)
 * some soldering skills, maybe :)

> **Disclaimer:** Under no circumstances shall I be liable or responsible for any damage you might cause to your property by use of the information contained in or linked from this web page.